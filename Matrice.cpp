#include "Matrice.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/**********************************************************************************************
Constructeur par défaut permettant d’initialiser une matrice
***********************************************************************************************
Entrée : rien
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est initialisé)
**********************************************************************************************/
template<typename T>
CMatrice<T>::CMatrice()
{
	nMATnb_Lignes = 0;
	nMATnb_Colonnes = 0;
	ptMATtab = NULL;
}


/**********************************************************************************************
Constructeur à deux arguments permettant d’initialiser une matrice
***********************************************************************************************
Entrée : un nombre de lignes, un nombre de colonnes
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est initialisé)
**********************************************************************************************/
template<typename T>
CMatrice<T>::CMatrice(unsigned int nNb_Lignes, unsigned int nNb_Colonnes)
{
	nMATnb_Lignes = nNb_Lignes;
	nMATnb_Colonnes = nNb_Colonnes;
	ptMATtab = (T*)malloc(nMATnb_Lignes * nMATnb_Colonnes * sizeof(T)); // On alloue un tableau de type T de taille lignes*colonnes
}


/**********************************************************************************************
Constructeur permettant d’initialiser une matrice à partir d'un fichier
***********************************************************************************************
Entrée : le nom d'un fichier
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est initialisé) ou
		   (Exception MATFichier_Corrompu : le fichier est corrompu) ou
		   (Exception MATType_Matrice : le type de la matrice indiqué n'est pas "Double") ou
		   (Exception MATIndice_Hors_Borne : un indice est en dehors des bornes)
**********************************************************************************************/
template<typename T>
CMatrice<T>::CMatrice(char* sNom_Fichier)
{
	char* sLigneTxt = new char[1024]; // Chaîne de caractères qui va contenir les lignes du fichier 1 par 1
	char* sParcoursLigne; // 2ème chaîne de caractères qui va servir à découper sLigneTxt pour récupérer les infos importantes
	ifstream monFichier(sNom_Fichier);

	if (!monFichier.is_open())
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(MATFichier_Corrompu);
		throw EXCErreur;
	}
	else
	{
		unsigned int nBoucle_ligneTxt = 0; // Variable pour savoir à quelle ligne on se situe dans le fichier
		unsigned int nLigneMat = 0; // Variable pour savoir à quelle ligne on se situe dans la matrice

		while (monFichier.getline(sLigneTxt, 1024))
		{
			switch (nBoucle_ligneTxt) 
			/*Selon la ligne, soit on récupère les informations (type, lignes, colonnes)
			Soit on remplit la matrice*/
			{
			case 0:
				sParcoursLigne = strstr(sLigneTxt, "TypeMatrice=");
				if (strcmp(sParcoursLigne + 12, "double")) // Le sujet demande de gérer uniquement les matrices de double
				{
					CException EXCErreur;
					EXCErreur.EXCModifier_Valeur(MATType_Matrice);
					cout << sParcoursLigne << "\n";
					throw EXCErreur;
				}
				else
					break;

			case 1:
				sParcoursLigne = strstr(sLigneTxt, "NBLignes=");
				nMATnb_Lignes = stoi(sParcoursLigne + 9);
				break;

			case 2:
				sParcoursLigne = strstr(sLigneTxt, "NBColonnes=");
				nMATnb_Colonnes = stoi(sParcoursLigne + 11);
				break;

			case 3:
				ptMATtab = (double*)malloc(nMATnb_Lignes * nMATnb_Colonnes * sizeof(double));
				/*On profite de la ligne "Matrice=[" pour allouer le tableau avec les variables précédemment initialisées
				D'où l'importance de bien respecter la nomenclature du fichier*/
				break;

			default:
				if (sLigneTxt[0] != ']') // Quand cette ligne est atteinte, il ne faut plus essayer de remplir la matrice
				{
					unsigned int nBoucle_i = 0;
					char* sContexte = NULL; // pas de réelle utilité, uniquement pour respecter les paramètres de strtok_s()
					sParcoursLigne = strtok_s(sLigneTxt, " ", &sContexte);
					while (sParcoursLigne != NULL && nBoucle_i < nMATnb_Colonnes)
					{
						try {
							this->MATModifier_Tab(nLigneMat * nMATnb_Colonnes + nBoucle_i, stod(sParcoursLigne));
							/*stod va ici transformer le contenu de sParcoursLigne en double
							Si on avait du gérer d'autres types que double, il aurait fallu faire un switch
							en fonction du type de la matrice et utiliser les différentes fonctions (stoi, stof, ...)*/
						}
						catch (CException EXCErreur) {
							if (EXCErreur.EXCLire_Valeur() == MATIndice_Hors_Borne)
							{
								printf("Erreur, l'indice depasse des bornes du tableau.\n");
								throw EXCErreur;
							}
						}
						nBoucle_i++;
						sParcoursLigne = strtok_s(NULL, " ", &sContexte);
					}
					nLigneMat++;
				}
				break;
			}
			nBoucle_ligneTxt++;
		}
	}
}


/**********************************************************************************************
Constructeur de recopie permettant d’initialiser une matrice
***********************************************************************************************
Entrée : une matrice
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est initialisé) ou
		   (Exception MATIndice_Hors_Borne : l'indice donné est en dehors des bornes)
**********************************************************************************************/
template<typename T>
CMatrice<T>::CMatrice(CMatrice<T> & MATMatrice)
{
	nMATnb_Lignes = MATMatrice.nMATnb_Lignes;
	nMATnb_Colonnes = MATMatrice.nMATnb_Colonnes;
	ptMATtab = (T*)malloc(nMATnb_Lignes * nMATnb_Colonnes * sizeof(T));

	// On essaie de recopier le contenu du tableau de MATMatrice dans celui de notre objet
	for (unsigned int nBoucle_i = 0; nBoucle_i < nMATnb_Lignes; nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < nMATnb_Colonnes; nBoucle_j++)
		{
			try {
				unsigned int nPosition = nMATnb_Colonnes * nBoucle_i + nBoucle_j;
				this->MATModifier_Tab(nPosition, MATMatrice.MATLire_Tab(nPosition));
			}
			catch (CException EXCErreur) {
				if (EXCErreur.EXCLire_Valeur() == MATIndice_Hors_Borne)
				{
					printf("Erreur, l'indice dépasse des bornes du tableau.\n");
					throw EXCErreur;
				}
			}
		}
	}
}


/**********************************************************************************************
Méthode MATAfficher permettant d'afficher la matrice
***********************************************************************************************
Entrée : rien
Nécessite : néant
Sortie : Affichage console
Entraîne : (Affichage console de la matrice) ou
		   (Exception MATIndice_Hors_Borne : un indice est en dehors des bornes)
**********************************************************************************************/
template<typename T>
void CMatrice<T>::MATAfficher()
{
	for (unsigned int nBoucle_i = 0; nBoucle_i < MATLire_Lignes(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < MATLire_Colonnes(); nBoucle_j++)
		{
			try {
				cout << MATLire_Tab(MATLire_Colonnes() * nBoucle_i + nBoucle_j) << " ";
			}
			catch (CException EXCErreur) {
				if (EXCErreur.EXCLire_Valeur() == MATIndice_Hors_Borne)
				{
					printf("Erreur, l'indice depasse des bornes du tableau.\n");
					throw EXCErreur;
				}
			}
		}
		cout << "\n";
	}
}


/**********************************************************************************************
Méthode MATModifier_Colonnes permettant de modifier le nombres de colonnes de la matrice
***********************************************************************************************
Entrée : le nombres de colonnes
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est modifié) ou
		   (Exception MATIndice_Hors_Borne : l'indice donné est en dehors des bornes)
**********************************************************************************************/
template<typename T>
void CMatrice<T>::MATModifier_Colonnes(unsigned int nNb_Colonnes)
{
	nMATnb_Colonnes = nNb_Colonnes;
}


/**********************************************************************************************
Méthode MATModifier_Lignes permettant de modifier le nombres de lignes de la matrice
***********************************************************************************************
Entrée : le nombres de lignes
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est modifié)
**********************************************************************************************/
template<typename T>
void CMatrice<T>::MATModifier_Lignes(unsigned int nNb_Lignes)
{
	nMATnb_Lignes = nNb_Lignes;
}


/**********************************************************************************************
Méthode MATModifier_Tab permettant de modifier un élément de la matrice
***********************************************************************************************
Entrée : la position de l'élément, un élément
Nécessite : néant
Sortie : rien
Entraîne : (L’objet en cours est modifié) ou
		   (Exception MATIndice_Hors_Borne : l'indice donné est en dehors des bornes)
**********************************************************************************************/
template<typename T>
void CMatrice<T>::MATModifier_Tab(unsigned int nPosition, T tElement)
{
	if (nPosition < 0 || nPosition >= nMATnb_Lignes * nMATnb_Colonnes)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(MATIndice_Hors_Borne);
		throw EXCErreur;
	}
	else
	{
		ptMATtab[nPosition] = tElement;
	}
}


/**********************************************************************************************
Methode MATLire_Colonnes permettant de lire le nombre de colonnes de la matrice
***********************************************************************************************
Entrée :  rien
Nécessite : néant
Sortie : le nombre de colonnes de la matrice
Entraîne : (Retourne un entier non signé)
**********************************************************************************************/
template<typename T>
unsigned int CMatrice<T>::MATLire_Colonnes()
{
	return nMATnb_Colonnes;
}


/**********************************************************************************************
Methode MATLire_Tab permettant de lire le nombre de lignes de la matrice
***********************************************************************************************
Entrée : rien
Nécessite : néant
Sortie : le nombre de lignes de la matrice
Entraîne : (Retourne un entier non signé)
**********************************************************************************************/
template<typename T>
unsigned int CMatrice<T>::MATLire_Lignes()
{
	return nMATnb_Lignes;
}


/**********************************************************************************************
Methode MATLire_Tab permettant de lire un élément de la matrice
***********************************************************************************************
Entrée : la position de l'élément
Nécessite : néant
Sortie : un élément du tableau
Entraîne : (Retourne un élément de type T) ou
		   (Exception MATIndice_Hors_Borne : l'indice donné est en dehors des bornes)
**********************************************************************************************/
template<typename T>
T CMatrice<T>::MATLire_Tab(unsigned int nPosition)
{
	if (nPosition < 0 || nPosition >= nMATnb_Lignes * nMATnb_Colonnes)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(MATIndice_Hors_Borne);
		throw EXCErreur;
	}
	else
	{
		return ptMATtab[nPosition];
	}
}

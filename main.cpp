#include <iostream>
#include <fstream>
#include "Matrice.h"
#include "Operation.h"


using namespace std;


int main(int argc, char* argv[])
{
	
	if (argc < 2) {
		cout << "Veuillez donner des fichiers en argument pour pouvoir creer des matrices ! \n";
		return 1;
	}

	cout << argc-1 << " fichier(s) en argument, creation de la (ou des) matrice(s)... \n";
	cout << "\n";

	unsigned int nNb_Argument = argc;

	// On d�clare et initialise un tableau de matrices de taille �gale au nombre d'arguments
	CMatrice<double> *pMATTab_De_Matrices;
	pMATTab_De_Matrices = new CMatrice<double>[argc];

	//	Cr�ation et affichage de toutes les matrices 
	for (unsigned int nBoucle_i = 0; nBoucle_i < nNb_Argument-1; nBoucle_i++)
	{
		cout << "Matrice "<< nBoucle_i + 1 << " initialisee : \n";

		try {
			pMATTab_De_Matrices[nBoucle_i] = CMatrice<double>(argv[nBoucle_i + 1]); // On utilise le constructeur de matrice avec le nom du fichier en argument
		}
		catch (CException EXCErreur) {
			if (EXCErreur.EXCLire_Valeur() == MATFichier_Corrompu) {
				printf("Erreur, fichier corrompu. Ouverture impossible.\n");
				return 1;
			}
			if (EXCErreur.EXCLire_Valeur() == MATType_Matrice) {
				printf("Erreur, type de la matrice different de double.\n");
				return 1;
			}
			if (EXCErreur.EXCLire_Valeur() == MATIndice_Hors_Borne) {
				printf("Erreur, indice hors des bornes du tableau.\n");
				return 1;
			}
		}

		pMATTab_De_Matrices[nBoucle_i].MATAfficher(); // On affiche la matrice
		cout << "\n";
	}

	// On demande � l'utilisateur une constante num�rique
	double rConstante;

	cout << "Entrez une constante numerique : \n";
	cin >> rConstante;

	while (cin.fail()) // Utilisateur n'a pas entr�e de nombre 
	{ 
		cin.clear(); 
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // On ignore la valeur rentr�e

		// On redemande � l'utilisateur 
		cout << "Ce n'est pas constante numerique, veuillez reessayer : \n";
		cin >> rConstante;
	}
	cout << "\n";


	/*------------------------------------------------------------------Op�rations �l�mentaires------------------------------------------------------------------*/

	// Multiplication par une constante
	unsigned int nBoucle = 0;

	while(nBoucle < nNb_Argument-1)
	{
		cout << "Multiplication par " << rConstante <<" de la matrice " << nBoucle + 1 << " :\n";
		COperationMatrice<double>::OPMMultiplication(pMATTab_De_Matrices[nBoucle], rConstante).MATAfficher(); // On fait l'op�ration et on affiche la matrice r�sultat
		nBoucle++;
		cout << "\n";
	}

	// Division par une constante
	nBoucle = 0;

	while (nBoucle < nNb_Argument - 1)
	{
		cout << "Division par " << rConstante << " de la matrice " << nBoucle + 1 << " :\n";
		try {
			COperationMatrice<double>::OPMDivision(pMATTab_De_Matrices[nBoucle], rConstante).MATAfficher(); // On fait l'op�ration et on affiche la matrice r�sultat
		} catch (CException EXCErreur) {
			if (EXCErreur.EXCLire_Valeur() == OPMDivision_Par_Zero) {
				printf("Erreur, on ne peut pas diviser par 0.\n");
				return 1;
			}
		}
		nBoucle++;
		cout << "\n";
	}

	// Transpos�e 
	nBoucle = 0;

	while (nBoucle < nNb_Argument - 1)
	{
		cout << "Transposee de la matrice " << nBoucle + 1 << " : \n";
		COperationMatrice<double>::OPMTransposee(pMATTab_De_Matrices[nBoucle]).MATAfficher(); // On fait l'op�ration et on affiche la matrice r�sultat
		nBoucle++;
		cout << "\n";
	}



	/*------------------------------------------------------------------Op�rations complexes------------------------------------------------------------------*/

	// Si plus d'un fichier est pass�e en argument
	if (argc > 2) { 
		
		// Addition de toutes les matrices
		CMatrice<double> MATTmp(pMATTab_De_Matrices[0]); // Un matrice temporaire initialis� par la premi�re matrice du tableau

		nBoucle = 1;
		cout << "Addition de toutes les matrices : \n";

		while (nBoucle < nNb_Argument - 1)
		{
			try {
			MATTmp = COperationMatrice<double>::OPMAddition(MATTmp, pMATTab_De_Matrices[nBoucle]); // On stocke dans la matrice temporaire le r�sultat de l'op�ration � la position nBoucle et nBoucle+1 du tableau de matrices
			}
			catch (CException EXCErreur) {
				if (EXCErreur.EXCLire_Valeur() == OPMAddition_Impossible) {
					printf("Erreur, on ne peut pas additionner 2 matrices de taille diff�rente.\n");
					return 1;
				}
			}
			nBoucle++;
		}

		MATTmp.MATAfficher(); 
		cout << "\n";


		// Soustraction de toutes les matrices
		CMatrice<double> MATTmp2(pMATTab_De_Matrices[0]); // Un matrice temporaire initialis� par la premi�re matrice du tableau

		nBoucle = 1;
		cout << "Soustraction de toutes les matrices : \n";

		while (nBoucle < nNb_Argument - 1)
		{
			try {
				MATTmp2 = COperationMatrice<double>::OPMSoustraction(MATTmp2, pMATTab_De_Matrices[nBoucle]); // On stocke dans la matrice temporaire le r�sultat de l'op�ration � la position nBoucle et nBoucle+1 du tableau de matrices
			}
			catch (CException EXCErreur) {
				if (EXCErreur.EXCLire_Valeur() == OPMSoustraction_Impossible) {
					printf("Erreur, on ne peut pas soustraire 2 matrices de taille diff�rente.\n");
					return 1;
				}
			}
			nBoucle++;
		}

		MATTmp2.MATAfficher();
		cout << "\n";


		// Produit de toutes les matrices
		CMatrice<double> MATTmp3(pMATTab_De_Matrices[0]); // Un matrice temporaire initialis� par la premi�re matrice du tableau

		nBoucle = 1;
		cout << "Produit de toutes les matrices : \n";

		while (nBoucle < nNb_Argument - 1)
		{
			try {
				MATTmp3 = COperationMatrice<double>::OPMProduit(MATTmp3, pMATTab_De_Matrices[nBoucle]);  // On stocke dans la matrice temporaire le r�sultat de l'op�ration � la position nBoucle et nBoucle+1 du tableau de matrices
			}
			catch (CException EXCErreur) {
				if (EXCErreur.EXCLire_Valeur() == OPMProduit_Impossible) {
					printf("Erreur, on ne peut pas multiplier 2 matrices de taille differente.\n");
					return 1;
				}
			}
			nBoucle++;
		}

		MATTmp3.MATAfficher();
		cout << "\n";

	}

	delete(pMATTab_De_Matrices);

	return 0;
	
}
#pragma once
#ifndef CMATRICE
#define CMATRICE

#include "CException.h"

#define MATIndice_Hors_Borne 14
#define MATFichier_Corrompu 15
#define MATType_Matrice 16

template<typename T>
class CMatrice
{
	private : 
		unsigned int nMATnb_Lignes;
		unsigned int nMATnb_Colonnes;
		T* ptMATtab; // Tableau � une dimension qui stocke les �l�ments de la matrice 

	public :

		/**********************************************************************************************
		Constructeur par d�faut permettant d�initialiser une matrice
		***********************************************************************************************
		Entr�e : rien
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est initialis�)
		**********************************************************************************************/
		CMatrice();


		/**********************************************************************************************
		Constructeur � deux arguments permettant d�initialiser une matrice
		***********************************************************************************************
		Entr�e : un nombre de lignes, un nombre de colonnes
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est initialis�)
		**********************************************************************************************/
		CMatrice(unsigned int nNb_Lignes, unsigned int nNb_Colonnes);


		/**********************************************************************************************
		Constructeur de recopie permettant d�initialiser une matrice
		***********************************************************************************************
		Entr�e : une matrice 
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est initialis�) ou
				   (Exception MATIndice_Hors_Borne : l'indice donn�e est en dehors des bornes)
		**********************************************************************************************/
		CMatrice(CMatrice <T> & MATMatrice);


		/**********************************************************************************************
		Constructeur permettant d�initialiser une matrice � partir d'un fichier
		***********************************************************************************************
		Entr�e : le nom d'un fichier
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est initialis�) ou
				   (Exception MATFichier_Corrompu : le fichier est corrompu) ou
				   (Exception MATType_Matrice : le type de la matrice indiqu� n'est pas "Double") ou
				   (Exception MATIndice_Hors_Borne : un indice est en dehors des bornes)
		**********************************************************************************************/
		CMatrice(char* sNom_Fichier);


		/**********************************************************************************************
		M�thode MATAfficher permettant d'afficher la matrice
		***********************************************************************************************
		Entr�e : rien
		N�cessite : n�ant
		Sortie : Affichage console
		Entra�ne : (Affichage console de la matrice) ou
				   (Exception MATIndice_Hors_Borne : un indice est en dehors des bornes)
		**********************************************************************************************/
		void MATAfficher();


		/**********************************************************************************************
		M�thode MATModifier_Colonnes permettant de modifier le nombres de colonnes de la matrice
		***********************************************************************************************
		Entr�e : le nombres de colonnes 
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est modifi�) ou
				   (Exception MATIndice_Hors_Borne : l'indice donn�e est en dehors des bornes)
		**********************************************************************************************/
		inline void MATModifier_Colonnes(unsigned int nNb_Colonnes);


		/**********************************************************************************************
		M�thode MATModifier_Lignes permettant de modifier le nombres de lignes de la matrice
		***********************************************************************************************
		Entr�e : le nombres de lignes 
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est modifi�) 
		**********************************************************************************************/
		inline void MATModifier_Lignes(unsigned int nNb_Lignes);


		/**********************************************************************************************
		M�thode MATModifier_Tab permettant de modifier un �l�ment de la matrice
		***********************************************************************************************
		Entr�e : la position de l'�l�ment, un �l�ment 
		N�cessite : n�ant
		Sortie : rien
		Entra�ne : (L�objet en cours est modifi�) ou 
				   (Exception MATIndice_Hors_Borne : l'indice donn�e est en dehors des bornes)
		**********************************************************************************************/
		inline void MATModifier_Tab(unsigned int nPosition, T tElement);


		/**********************************************************************************************
		Methode MATLire_Colonnes permettant de lire le nombre de colonnes de la matrice
		***********************************************************************************************
		Entr�e :  rien
		N�cessite : n�ant
		Sortie : le nombre de colonnes de la matrice
		Entra�ne : (Retourne un entier non sign�)
		**********************************************************************************************/
		inline unsigned int MATLire_Colonnes();


		/**********************************************************************************************
		Methode MATLire_Tab permettant de lire le nombre de lignes de la matrice
		***********************************************************************************************
		Entr�e : rien
		N�cessite : n�ant
		Sortie : le nombre de lignes de la matrice
		Entra�ne : (Retourne un entier non sign�)
		**********************************************************************************************/
		inline unsigned int MATLire_Lignes();


		/**********************************************************************************************
		Methode MATLire_Tab permettant de lire un �l�ment de la matrice 
		***********************************************************************************************
		Entr�e : la position de l'�l�ment
		N�cessite : n�ant
		Sortie : un �l�ment du tableau
		Entra�ne : (Retourne un �l�ment de type T) ou 
				   (Exception MATIndice_Hors_Borne : l'indice donn�e est en dehors des bornes)
		**********************************************************************************************/
		inline T MATLire_Tab(unsigned int nPosition);
};

#include "Matrice.cpp"

#endif
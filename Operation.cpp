#include <iostream>


/**********************************************************************************************
Methode statique OPMMultiplication permettant de faire la multiplication
d'une matrice par une constante
***********************************************************************************************
Entr�e : une constante
N�cessite : n�ant
Sortie : une matrice r�sultat de la multiplication
Entra�ne : (Retourne une matrice)
**********************************************************************************************/
template<typename T>
CMatrice<T> COperationMatrice<T>::OPMMultiplication(CMatrice<T> & MATMatrice, const T & tConstante)
{
	CMatrice<T> MATResultat(MATMatrice.MATLire_Lignes(), MATMatrice.MATLire_Colonnes());

	for (unsigned int nBoucle_i = 0; nBoucle_i < MATResultat.MATLire_Lignes(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < MATResultat.MATLire_Colonnes(); nBoucle_j++)
		{
			unsigned int nPosition = MATMatrice.MATLire_Colonnes() * nBoucle_i + nBoucle_j;
			MATResultat.MATModifier_Tab(nPosition, MATMatrice.MATLire_Tab(nPosition) * tConstante);
		}
	}
	return MATResultat;
}


/**********************************************************************************************
Methode statique OPMDivision permettant de faire la division d'une matrice par une constante
***********************************************************************************************
Entr�e : une constante
N�cessite : n�ant
Sortie : une matrice r�sultat de la division
Entra�ne : (Retourne une matrice) ou
		   (Exception OPMDivision_Par_Zero : division par zero impossible)
**********************************************************************************************/
template<typename T>
CMatrice<T> COperationMatrice<T>::OPMDivision(CMatrice<T>& MATMatrice, const T& tConstante)
{
	if (tConstante == 0)
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(OPMDivision_Par_Zero);
		throw EXCErreur;
	}
	else
	{
		CMatrice<T> MATResultat(MATMatrice.MATLire_Lignes(), MATMatrice.MATLire_Colonnes());

		for (unsigned int nBoucle_i = 0; nBoucle_i < MATResultat.MATLire_Lignes(); nBoucle_i++)
		{
			for (unsigned int nBoucle_j = 0; nBoucle_j < MATResultat.MATLire_Colonnes(); nBoucle_j++)
			{
				unsigned int nPosition = MATMatrice.MATLire_Colonnes() * nBoucle_i + nBoucle_j;
				MATResultat.MATModifier_Tab(nPosition, MATMatrice.MATLire_Tab(nPosition) / tConstante);
			}
		}
		return MATResultat;
	}
}



/**********************************************************************************************
Methode statique OPMTransposee permettant de faire la transpos�e d'une matrice
***********************************************************************************************
Entr�e : un matrice
N�cessite : n�ant
Sortie : une matrice r�sultat de la transpos�e
Entra�ne : (Retourne une matrice)
**********************************************************************************************/
template <typename T>
CMatrice<T> COperationMatrice<T>::OPMTransposee(CMatrice<T>& MATMatrice)
{
	CMatrice<T> MATResultat(MATMatrice.MATLire_Colonnes(), MATMatrice.MATLire_Lignes());

	for (unsigned int nBoucle_i = 0; nBoucle_i < MATResultat.MATLire_Lignes(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < MATResultat.MATLire_Colonnes(); nBoucle_j++)
		{
			unsigned int nPosition = MATResultat.MATLire_Colonnes() * nBoucle_i + nBoucle_j;
			MATResultat.MATModifier_Tab(nPosition, MATMatrice.MATLire_Tab(MATMatrice.MATLire_Colonnes()*nBoucle_j + nBoucle_i));
		}
	}
	return MATResultat;
}


/**********************************************************************************************
Methode statique OPMAddition permettant de faire l'addition de deux matrices
***********************************************************************************************
Entr�e : deux matrices
N�cessite : n�ant
Sortie : une matrice r�sultat de l'addition
Entra�ne : (Retourne une matrice) ou
		   (Exception OPMAddition_Impossible : addition des matrices impossible,
											   le nombre de colonnes et de lignes
											   des deux matrices doit �tre �gal)
**********************************************************************************************/
template <typename T>
CMatrice<T> COperationMatrice<T>::OPMAddition(CMatrice<T>& MATMatrice1, CMatrice<T>& MATMatrice2)
{
	if ((MATMatrice1.MATLire_Colonnes() != MATMatrice2.MATLire_Colonnes()) || (MATMatrice1.MATLire_Lignes() != MATMatrice2.MATLire_Lignes()))
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(OPMAddition_Impossible);
		throw EXCErreur;
	}

	CMatrice<T> MATResultat(MATMatrice1.MATLire_Lignes(), MATMatrice1.MATLire_Colonnes());

	for (unsigned int nBoucle_i = 0; nBoucle_i < MATResultat.MATLire_Lignes(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < MATResultat.MATLire_Colonnes(); nBoucle_j++)
		{
			unsigned int nPosition = MATResultat.MATLire_Colonnes() * nBoucle_i + nBoucle_j;
			MATResultat.MATModifier_Tab(nPosition, MATMatrice1.MATLire_Tab(nPosition) + MATMatrice2.MATLire_Tab(nPosition));
		}
	}

	return MATResultat;
}


/**********************************************************************************************
Methode statique OPMSoustraction permettant de faire la soustraction de deux matrices
***********************************************************************************************
Entr�e : deux matrices
N�cessite : n�ant
Sortie : une matrice r�sultat de la soustraction
Entra�ne : (Retourne une matrice) ou
		   (Exception OPMSoustraction_Impossible : soustraction des matrices impossible,
												   le nombre de colonnes et de lignes
												   des deux matrices doit �tre �gal)
**********************************************************************************************/
template <typename T>
CMatrice<T> COperationMatrice<T>::OPMSoustraction(CMatrice<T>& MATMatrice1, CMatrice<T>& MATMatrice2)
{
	if ((MATMatrice1.MATLire_Colonnes() != MATMatrice2.MATLire_Colonnes()) || (MATMatrice1.MATLire_Lignes() != MATMatrice2.MATLire_Lignes()))
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(OPMSoustraction_Impossible);
		throw EXCErreur;
	}

	CMatrice<T> MATResultat(MATMatrice1.MATLire_Lignes(), MATMatrice1.MATLire_Colonnes());

	for (unsigned int nBoucle_i = 0; nBoucle_i < MATResultat.MATLire_Lignes(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < MATResultat.MATLire_Colonnes(); nBoucle_j++)
		{
			unsigned int nPosition = MATResultat.MATLire_Colonnes() * nBoucle_i + nBoucle_j;
			MATResultat.MATModifier_Tab(nPosition, MATMatrice1.MATLire_Tab(nPosition) - MATMatrice2.MATLire_Tab(nPosition));
		}
	}

	return MATResultat;
}


/**********************************************************************************************
Methode statique OPMProduit permettant de faire le produit de deux matrices
***********************************************************************************************
Entr�e : deux matrices
N�cessite : n�ant
Sortie : une matrice r�sultat du produit
Entra�ne : (Retourne une matrice) ou
		   (Exception OPMProduit_Impossible : produit des matrices impossible,
											  le nombre de colonnes de la premi�re matrice
											  doit �tre �gal au nombre de lignes de la deuxi�me)
**********************************************************************************************/
template <typename T>
CMatrice<T> COperationMatrice<T>::OPMProduit(CMatrice<T>& MATMatrice1, CMatrice<T>& MATMatrice2)
{
	if ((MATMatrice1.MATLire_Colonnes() != MATMatrice2.MATLire_Lignes()))
	{
		CException EXCErreur;
		EXCErreur.EXCModifier_Valeur(OPMProduit_Impossible);
		throw EXCErreur;
	}

	CMatrice<T> MATResultat(MATMatrice1.MATLire_Lignes(), MATMatrice2.MATLire_Colonnes());

	for (unsigned int nBoucle_i = 0; nBoucle_i < MATResultat.MATLire_Lignes(); nBoucle_i++)
	{
		for (unsigned int nBoucle_j = 0; nBoucle_j < MATResultat.MATLire_Colonnes(); nBoucle_j++)
		{
			unsigned int nPosition = MATResultat.MATLire_Colonnes() * nBoucle_i + nBoucle_j;
			T Tvaleur = MATMatrice1.MATLire_Tab(MATMatrice1.MATLire_Colonnes() * nBoucle_i) * MATMatrice2.MATLire_Tab(nBoucle_j);
			for (unsigned int nBoucle_k = 1; nBoucle_k < MATMatrice1.MATLire_Colonnes(); nBoucle_k++)
			{
				Tvaleur += MATMatrice1.MATLire_Tab(MATMatrice1.MATLire_Colonnes() * nBoucle_i + nBoucle_k) * MATMatrice2.MATLire_Tab(nBoucle_k * MATMatrice2.MATLire_Colonnes() + nBoucle_j);
			}

			MATResultat.MATModifier_Tab(nPosition, Tvaleur);
		}
	}

	return MATResultat;
}

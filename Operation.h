#pragma once
#ifndef COPERATIONMATRICE
#define COPERATIONMATRICE

#include "Matrice.h"
#include "CException.h"

#define OPMDivision_Par_Zero 10
#define OPMAddition_Impossible 11
#define OPMSoustraction_Impossible 12
#define OPMProduit_Impossible 13

template<typename T>
class COperationMatrice
{
public:

	/**********************************************************************************************
	Methode statique OPMMultiplication permettant de faire la multiplication
	d'une matrice par une constante
	***********************************************************************************************
	Entr�e : une constante
	N�cessite : n�ant
	Sortie : une matrice r�sultat de la multiplication
	Entra�ne : (Retourne une matrice) 
	**********************************************************************************************/
    static CMatrice<T> OPMMultiplication(CMatrice<T> & MATMatrice, const T & tConstante);


	/**********************************************************************************************
	Methode statique OPMDivision permettant de faire la division d'une matrice par une constante
	***********************************************************************************************
	Entr�e : une constante 
	N�cessite : n�ant
	Sortie : une matrice r�sultat de la division
	Entra�ne : (Retourne une matrice) ou
			   (Exception OPMDivision_Par_Zero : division par zero impossible)
	**********************************************************************************************/
    static CMatrice<T> OPMDivision(CMatrice<T> & MATMatrice, const T & tConstante);
  

	/**********************************************************************************************
	Methode statique OPMTransposee permettant de faire la transpos�e d'une matrice
	***********************************************************************************************
	Entr�e : un matrice
	N�cessite : n�ant
	Sortie : une matrice r�sultat de la transpos�e
	Entra�ne : (Retourne une matrice)
	**********************************************************************************************/
    static CMatrice<T> OPMTransposee(CMatrice<T> & MATMatrice);


	/**********************************************************************************************
	Methode statique OPMAddition permettant de faire l'addition de deux matrices
	***********************************************************************************************
	Entr�e : deux matrices
	N�cessite : n�ant
	Sortie : une matrice r�sultat de l'addition
	Entra�ne : (Retourne une matrice) ou
			   (Exception OPMAddition_Impossible : addition des matrices impossible,
												   le nombre de colonnes et de lignes 
												   des deux matrices doit �tre �gal)
	**********************************************************************************************/
    static CMatrice<T> OPMAddition(CMatrice<T>& MATMatrice1, CMatrice<T>& MATMatrice2);


	/**********************************************************************************************
	Methode statique OPMSoustraction permettant de faire la soustraction de deux matrices
	***********************************************************************************************
	Entr�e : deux matrices
	N�cessite : n�ant
	Sortie : une matrice r�sultat de la soustraction
	Entra�ne : (Retourne une matrice) ou
			   (Exception OPMSoustraction_Impossible : soustraction des matrices impossible,
												       le nombre de colonnes et de lignes 
												       des deux matrices doit �tre �gal)
	**********************************************************************************************/
    static CMatrice<T> OPMSoustraction(CMatrice<T>& MATMatrice1, CMatrice<T>& MATMatrice2);


	/**********************************************************************************************
	Methode statique OPMProduit permettant de faire le produit de deux matrices
	***********************************************************************************************
	Entr�e : deux matrices
	N�cessite : n�ant
	Sortie : une matrice r�sultat du produit
	Entra�ne : (Retourne une matrice) ou
			   (Exception OPMProduit_Impossible : produit des matrices impossible,
												  le nombre de colonnes de la premi�re matrice
												  doit �tre �gal au nombre de lignes de la deuxi�me)
	**********************************************************************************************/
    static CMatrice<T> OPMProduit(CMatrice<T>& MATMatrice1, CMatrice<T>& MATMatrice2);

};

#include "Operation.cpp"

#endif